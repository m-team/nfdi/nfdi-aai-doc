{% include 'logo.md' %}

# Authors

The NFDI Architecture was developed as an initiative to provide the AAI
Basis for all NFDI consortia. The panel consists of experts from a wide
spectrum of the German IAM / AAI landscape. All of the individuals are
involved in several NFDI consortia.

- Marcus Hardt ([KIT](https://kit.edu)): Developed the "AARC Blueprint Architectures", co-developed
    the "AARC Policy Development Kit" and several AARC-Guidelines. He also
    active in the EOSC Task Force on AAI. He contributed to the Helmholtz-AAI.

- Sander Apweiler ([FZJ](https://fz-juelich.de)):
    Operates several Instances of the Community AAI software `Unity`. This
    includes platforms such as Helmholtz-AAI, EUDAT B2ACCESS, and the
    PUNCH4NFDI instance. He also works in consulting communities with the
    design of the AAI and the integration of services and is active in the
    AEGIS group and EOSC Task Force on AAI. He is the contact point for
    Consortia that want to use Unity.
    He is the contact point for Consortia that want to use unity.

- Matthias Bonn ([KIT](https://kit.edu)): Contributes to operation and
    development of several instances of the
    [`regAPP`](https://www.scc.kit.edu/en/services/regapp.php) software,
    which can be used as Community AAI

    (bwIDM, FeLS), and as an Infrastructure Proxy simultaneously.
    He is the contact point for Consortia that want to use regAPP.

- Peter Gietz ([DAASI International GmbH](https://daasi.de)):  Co-Founder
    and CEO of DAASI International. He was involved in several projects on
    Virtual Research Environments and Infrastructures and represents the
    DARIAH AAI in several working groups, such as FIM4R. He is the contact
    point for Consortia that want to use
    ['didmos'](https://daasi.de/en/federated-identity-and-access-management/iam-solutions/didmos/),


- David Hübner ([DAASI International GmbH](https://daasi.de)): Co
    developed the "AARC Blueprint Architectures" and several
    AARC-Guidelines and lead the implementation of an AARC compliant proxy
    in the DARIAH AAI. He is also the product owner of DAASI
    International's IAM suite
    ['didmos'](https://daasi.de/en/federated-identity-and-access-management/iam-solutions/didmos/),
    which can be used to implement a Community AAI.

- Thorsten Michels ([TUK/RPTU](https://www.uni-kl.de)): is spokesperson of
    the [working group IAM of
    ZKI](https://www.zki.de/ueber-den-zki/arbeitskreise/arbeitskreis-identity-und-access-management/).
    He contributed his expertise in SAML Federations, privacy and
    IAM to the NFDI architecture.


- Wolfgang Pempe ([DFN](https://dfn.de)): DFN-AAI federation operator,
    member of the eduGAIN assembly and members' representative in
    the Shibboleth consortium board. He is one of the spokespersons of the
    [WG IAM](https://doi.org/10.5281/zenodo.6421866) in the NFDI Section Common Infrastructures, active in the EOSC
    Task Force AAI Architecture and contributed to several
    AARC-Guidelines.

- Christof Pohl ([GWDG](https://gwdg.de)): Leads the team that develops
    and provides the Community AAI software `AcademicID`. He is the
    contact point for Consortia that want to use AcademicID.

- Marius Politze ([RWTH Aachen](https://rwth-aachen.de)): He coordinates the
    [WG IAM](https://doi.org/10.5281/zenodo.6421866) in the NFDI Section Common
    Infrastructures. He is also architect of several federated research data
    services that are part of the DFN-AAI federation.


{% include 'gitinfo.md' %}
