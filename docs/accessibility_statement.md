{% include 'logo.md' %}

# Accessibility

The **Karlsruhe Institute of Technology (KIT)** is committed to making its website <https://doc.nfdi-aai.de/> accessible in accordance with **[Article 10, par. 1 of the State Equal Opportunities for People with Disabilities Act (L-BGG)](https://www.landesrecht-bw.de/jportal/?quelle=jlink&query=BehGleichStG+BW+%C2%A7+10&psml=bsbawueprod.psml&max=true)**.

The website <https://doc.nfdi-aai.de/> has been evaluated using a **self-assessment** with random sample tests. Conformance could only be determined for individual pages.

## Status of Compliance with the Requirements

The website <https://doc.nfdi-aai.de/> is partly in compliance with Article 10, par. 1, L-BGG.
The following content does not meet accessibility standards.

## Inaccessible Contents

The following content is **not fully accessible**:

- **Contrast issues**: The analysis revealed multiple contrast errors. In particular, turquoise text on a light gray background does not provide sufficient contrast.
- **Navigation issues with screen readers**: When navigating via the **link list**, some links are read aloud as **"#"**, which is misleading.
- **Policy table accessibility**: Although the **policy table** has an alternative text, it does not accurately convey the details of the graphic. There is no textual explanation **above or below the graphic**, making it difficult to understand for users with **visual impairments** or **color blindness**.

We are currently working on resolving these issues as soon as possible.

## Creation of this Accessibility Statement

This statement was created on **January 30, 2025**.

## Feedback and Contact Information

Have you noticed accessibility issues on <https://doc.nfdi-aai.de/>? Or do you have questions about accessibility?

The contents of our web appearance is to be equally accessible for people with a disability. In case you encounter inaccessibilities on our websites and in our mobile applications, help us: Send us an information and a description of the inaccessibility using the [contact form](https://www.kit.edu/kit/english/accessibility-contact.php) or by electronic mail [**internetredaktion@sts.kit.edu**](mailto:internetredaktion@sts.kit.edu).

You can also reach us by mail or telephone: 

**Karlsruhe Institute of Technology**  
Executive Office and Strategy
Corporate Communications
Kaiserstraße 12
76131 Karlsruhe, Germany
Phone: +49 (0)721/608-41105 
Email: [**info@kit.edu**](mailto:info@kit.edu)


## Enforcement Procedure

To ensure that the website meets the requirements outlined in Article 10, par. 1, L-BGG, contact us and give a feedback. The contact data can be found above. If you do not receive any reply from us within four weeks or if our reply is not satisfactory, contact the  Commissioner of the State Government for the Needs of People with Disabilities. 

The Commissioner of the State Government for the Needs of People with Disabilities can be reached as follows: 

**[Landes-Behindertenbeauftragte](https://sozialministerium.baden-wuerttemberg.de/de/ministerium/landes-behindertenbeauftragte/) (State Commissioner for the Needs of People with Disabilities):**  
Geschäftsstelle der Landes-Behindertenbeauftragten (Office of the State Commissioner for the Needs of People with Disabilities)
Phone: +49 (0)711/279-3360 
Else-Josenhans-Straße 6  
70173 Stuttgart, Germany  

Email: [**Poststelle@bfbmb.bwl.de**](mailto:Poststelle@bfbmb.bwl.de)  


Please note the right of collective regress according to Article 12, par. 1, cl. 1, No. 4, L-BGG.



{% include 'gitinfo.md' %}

