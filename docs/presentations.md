{% include 'logo.md' %}

# Presentations

<!-- New presentations go on top, please -->

## Infoshare
Here you can find our presentations from the IAM4NFDI info share on 04.06.2024.

- [:fontawesome-solid-file-powerpoint:](presentations/2406/Start_and_end_mod_wp.pptx "Powerpoint Presentation: Welcome Adress") Welcome Adress (Thorsten Michels)
- [:fontawesome-solid-file-powerpoint:](presentations/2406/IAM.pptx "Powerpoint Presentation: Introduction to IAM and AAI") Introduction to IAM and AAI (Wolfgang Pempe and Thorsten Michels)
- [:fontawesome-solid-file-pdf:](presentations/2406/federations_and_nfdi-aai.pdf "PDF Presentation: NFDI AAI Architecture") NFDI AAI Architecture (Wolfgang Pempe and Thorsten Michels)
- NFDI Community AAI Software Solutions 
    - [:fontawesome-solid-file-pdf:](presentations/2406/CIAM-didmos-v3.pdf "PDF Presentation: didmos") didmos (Peter Gietz)
    - [:fontawesome-solid-file-pdf:](presentations/2406/NFDI_CAAI-Workshop2_RegApp.pdf "PDF Presentation: RegApp") RegApp (Matthias Bonn)
    - [:fontawesome-solid-file-pdf:](presentations/2406/AcademicID_Short.pdf "PDF Presentation: AcademicID") AcademicID (Christof Pohl)
    - [:fontawesome-solid-file-pdf:](presentations/2406/unity_nfdi_infoshare2.pdf "PDF Presentation: Unity") Unity (Laura Hofer)
- [:fontawesome-solid-file-pdf:](presentations/2406/WP4-operations.pdf "PDF Presentation: Operating Concept") Operating Concept (Peter Gietz)
- [:fontawesome-solid-file-pdf:](presentations/2406/2024-06-04_IAM4NDI-Inkubatorprojekte.pdf "PDF Presentation: Incubator Process") Incubator Process (Marcel Nellesen)
- [:fontawesome-solid-file-pdf:](presentations/2406/IAM4NFDI_Incubator_NFDI4Earth_OpenedX_WorkshopDemo_20240604.pdf "PDF Presentation: Example of an Incubator") Example of an Incubator (Ralf Klammer)
- [:fontawesome-solid-file-pdf:](presentations/2406/policies.pdf "PDF Presentation: Policy Concept") Policy Concept (Wolfgang Pempe)

# Publications

## Nellesen, M. & Pempe, W. (2024). Er wächst und gedeiht – der NFDI-Basisdienst IAM4NFDI
- [:fontawesome-solid-file-pdf: DFN Mitteilungen, Ausgabe 106, 11-13 ](https://www.dfn.de/wp-content/uploads/2024/11/DFN_106_download-opt.pdf "PDFN Mitteilungen, Ausgabe 106")

# Downloads

## Proposals of the IAM4NFDI Project
- [:fontawesome-solid-file-pdf: **Integration** Phase](documents/iam4nfdi_integration.pdf "PDF: Integration Phase")
- [:fontawesome-solid-file-pdf: **Initialisation** Phase](documents/iam4nfdi_initialization.pdf "PDF: Initialisation Phase")

## Service description, operating concept and TOMs 
[Service description, operating concept and TOMs](https://drive.google.com/drive/folders/1nT0YpZfOZ9YxQ-qgsA16zZw5KJ0TnTgV)

## Workshops
We publish the material of our workshops on this [google
drive](https://drive.google.com/drive/folders/1nT0YpZfOZ9YxQ-qgsA16zZw5KJ0TnTgV).

## Login Buttons
You can download [NFDI Login Buttons for your service here](https://extfiles.dfn.de/index.php/s/EW2p8RepCyRmHZJ?).



{% include 'gitinfo.md' %}
