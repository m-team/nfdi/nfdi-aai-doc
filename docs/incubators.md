{% include 'logo.md' %}

# IAM4NFDI Incubators

[See present and past incubators at our incubator overview
page https://incubators.nfdi-aai.de](https://incubators.nfdi-aai.de)

Authentication and authorisation can be quite tricky and challenging when
handled individually. Why not leverage solutions that's already prepared,
tested, and approved? This approach reduces our responsibility, increases
comfort, and minimises the need to navigate delicate security aspects.

## How to apply

Applicants should use our template to apply for an incubator.

- [Current Call for Applications](https://pad.otc.coscine.dev/s/X_1mk4sKD)
- [Template for IAM4NFDI Incubators](https://extfiles.dfn.de/index.php/s/sYHXnnpzgp3TEaC/download)
- [IAM4NFDI Incubator Process](https://extfiles.dfn.de/index.php/s/eDzxzbdbdoGtiPb/download)




{% include 'gitinfo.md' %}
