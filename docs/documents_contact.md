{% include 'logo.md' %}

# Contact

## Mailinglist
To stay connected and be informed about updates and events, you should
[subscribe to the NFDI-AAI-INFO mailinglist here](https://www.lists.kit.edu/sympa/subscribe/nfdi-aai-info?previous_action=info).

## Consulting
Explore our comprehensive identity and access management solutions for the NFDI, and contact us today to discuss your specific requirements. You can reach the IAM4NFDI project via [aai-kernteam@lists.kit.edu](mailto:aai-helpdesk@list.nfdi.de). 

{% include 'gitinfo.md' %}
