{% include 'logo.md' %}

# NFDI Infrastructure Proxy

The Infrastructure Proxy is used to connect services that address cross-community use cases, i.e. to make services available to users from the entire NFDI. Accoring to the NFDI-AAI Architecture, the Infrastructure Proxy is supposed to be connected to all Community AAI instances. It therefore brings together user information from different sources: 
- Home IdPs from Identity Federations (DFN-AAI and eduGAIN)
- ORCID and Social Login
- DFN edu-ID
- Guest IdPs
- VO membership and resource capability information (Community AAIs)

The Infrastructure Proxy is operated by DFN-CERT on behalf of DFN. For all questions about the Infrastructure Proxy please contact the DFN-AAI hotline (see below)

## Checklist for connecting services to the Infrastructure Proxy

### Technical Aspects
- Make sure that the service supports and is able to consume the Community Attribute Profiles specified in the current version of the [Infrastructure Attribute Policy](policies.md#nfdi-policies) 
- The connection to the infra proxy is always established via OpenID Connect:
    - The configuration URL of the Proxy's OP component: [https://infraproxy.nfdi-aai.dfn.de/.well-known/openid-configuration](https://infraproxy.nfdi-aai.dfn.de/.well-known/openid-configuration)
    - For registering a service, we'd need at least one **redirect_uri**, in exchange for a **client_id** and a **client_secret**. 
    - The final integration is usually done in an interactive session (video conference).

### Data Protection, Formal Aspects
- Please provide at least a German privacy statement for the service. There are German and English templates available as part of the [NFDI-AAI policy framework](policies.md)
- As part of the policy framework, there are also [templates available](policies.md) for Service Acceptable Use and Service Access Policies, which are both optional. If you're planning to make use of such documents, please let us know.
- Provide a security contact for the service and register the email address with the [NFDI-AAI Security List](https://www.listserv.dfn.de/sympa/subscribe/nfdi-aai-security), which is used as incident response information channel.
- As official operator of the Infrastructure Proxy, the DFN acts as data processor ("Auftragsverarbeiter") for the operator of the service. A ready-made Data Processing Agreement ("Auftragsverarbeitungsvereinbarung") exists for this purpose. The DFN-Team takes care of the paperwork.  

## Contact information
- E-Mail: hotline@aai.dfn.de
- Phone:  +49-30-884299-9124 

{% include 'gitinfo.md' %}
