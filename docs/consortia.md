{% include 'logo.md' %}

# NFDI Consortia: Overview Community AAI Usage

Here we list the decisions of each NFDI Consortia, which Community AAI to
use.


| Consortium                              |       Academic ID       |          didmos         |          RegApp         |          unity          |          other          |
|-----------------------------------------|:-----------------------:|:-----------------------:|:-----------------------:|:-----------------------:|:-----------------------:|
| BERD@NFDI                               |                         |                         |                         |                         |                         |
| <span class='bg_blue'>DAPHNE4NFDI       |                         |                         |                         | <span class='bg_blue'>x |                         |
| <span class='bg_blue'>DataPLANT         |                         |                         |                         |                         | <span class='bg_blue'>x |
| FAIRagro                                |                         |                         |                         |                         |                         |
| <span class='bg_blue'>FAIRmat                                 |                         |                         |                         | <span class='bg_blue'>x                        |                         |
| <span class='bg_blue'>GHGA              |                         |                         |                         |                         | <span class='bg_blue'>x |
| KonsortSWD                              |                         |                         |                         |                         |                         |
| <span class='bg_blue'>MaRDI             |                         | <span class='bg_blue'>x |                         |                         |                         |
| <span class='bg_blue'>NFDI4Biodiversity |                         |                         |                         |                         | <span class='bg_blue'>x |
| NFDI4BIOIMAGE                           |                         |                         |                         |                         |                         |
| <span class='bg_blue'>NFDI4Cat          |                         |                         | <span class='bg_blue'>x |                         |                         |
| <span class='bg_blue'>NFDI4Chem         |                         |                         | <span class='bg_blue'>x |                         |                         |
| <span class='bg_blue'>NFDI4Culture      |                         | <span class='bg_blue'>x |                         |                         |                         |
| <span class='bg_blue'>NFDI4DataScience  |                         |                         | <span class='bg_blue'>x |                         |                         |
| <span class='bg_blue'>NFDI4Earth        | <span class='bg_blue'>x |                         |                         |                         |                         |
| <span class='bg_blue'>NFDI4Energy       |                         |                         | <span class='bg_blue'>x |                         |                         |
| NFDI4Health                             |                         |                         |                         |                         |                         |
| <span class='bg_blue'>NFDI4Immuno       |                         |                         |                         | <span class='bg_blue'>x |                         |
| <span class='bg_blue'>NFDI4Ing          |                         |                         | <span class='bg_blue'>x |                         |                         |
| NFDI4Memory                             |                         |                         |                         |                         |                         |
| NFDI4Microbiota                         |                         |                         |                         |                         |                         |
| <span class='bg_blue'>NFDI4Objects      |                         | <span class='bg_blue'>x |                         |                         |                         |
| <span class='bg_blue'>NFDI-MatWerk      |                         |                         | <span class='bg_blue'>x |                         |                         |
| <span class='bg_blue'>NFDIxCS           | <span class='bg_blue'>x |                         |                         |                         |                         |
| <span class='bg_blue'>PUNCH4NFDI        |                         |                         |                         | <span class='bg_blue'>x |                         |
| <span class='bg_blue'>Text+             | <span class='bg_blue'>x |                         |                         |                         |                         |

<style>
    .bg_red { background-color: #ff9999; width: 100%; height: 100%; display: block; }
    .bg_gray { background-color: #888888; width: 100%; height: 100%; display: block; }
    .bg_light_red { background-color: #bb4444; width: 100%; height: 100%; display: block; }
    .bg_very_red { background-color: #ff0000; width: 100%; height: 100%; display: block; }
    .bg_blue { background-color: #0abaf0; width: 100%; height: 100%; display: block; }
.md-typeset table:not([class]) tbody tr:hover {
    background-color: rgba(10,10,10,.1);
}
th, td {
    border: 1px solid var(--md-typeset-table-color);
    border-spacing: 0;
    border-bottom: none;
    border-left: none;
    border-top: none;
}

.md-typeset__table {
    line-height: 1.3;
}

.md-typeset__table table:not([class]) {
    font-size: .74rem;
    border-right: none;
}

.md-typeset__table table:not([class]) td,
.md-typeset__table table:not([class]) th {
    padding: 0px;
}

/* light mode alternating table bg colors */
.md-typeset__table tr:nth-child(2n) {
    background-color: #f0f0f0;
}

/* dark mode alternating table bg colors */
[data-md-color-scheme="slate"] .md-typeset__table tr:nth-child(2n) {
    background-color: hsla(var(--md-hue),25%,25%,1)
}
</style>

<!-- :let g:table_mode_corner_corner='|' -->

{% include 'gitinfo.md' %}
