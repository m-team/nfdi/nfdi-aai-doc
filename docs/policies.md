{% include 'logo.md' %}

# Policies and Templates

Our policy table gives an overview who needs to read or write which policy

[![Matrix which explains which entities define (rows) and comply to (columns) the different policy documents introduced on this page. Refer to the individual documents for details.](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.3/raw/images/tabelle_policy-framework.png?job=build-docs "policy_table"){: style="height:280px"}](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.3/raw/images/tabelle_policy-framework.png?job=build-docs)


## NFDI Policies

<small>( Current version: 0.9 )</small>

These Policies are defined by NFDI. All members must follow these
policies:

- Top Level Policy [:fontawesome-solid-file-pdf:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/00_Top-Level-Policy.pdf?job=build-docs "PDF: Top Level Policy")
- VO Membership Management Policy (VOMMP): [:fontawesome-solid-file-pdf:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/01_CAAI-VOMMP.pdf?job=build-docs "PDF: VO Membership Management Policy (VOMMP)")
- Security Incident Response Proecedure (SIRP): [:fontawesome-solid-file-pdf:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/02_SIRP.pdf?job=build-docs "PDF: Security Incident Response Proecedure (SIRP)")
- Policy on Processing of Personal Data (PPPD): [:fontawesome-solid-file-pdf:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/03_PPPD.pdf?job=build-docs "PDF: Policy on Processing of Personal Data (PPPD)")
- Infrastructure Attribute Policy (IAP): [:fontawesome-solid-file-pdf:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/04_IAP.pdf?job=build-docs "PDF: Infrastructure Attribute Policy (IAP)")


## NFDI Policy Templates

<small>( Current version: 0.9.4 )</small>

These templates are defined by NFDI. They MAY be used as a template for
defining own policies. (In general these templates make it safer to follow
existing regulations than self-written policies.)

The documents can be edited with Libreoffice or compatible Software.

- Community AAI Acceptable Use Policy (CAAI-AUP):
    (english: [:fontawesome-solid-file-pdf:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05-CAAI_AUP.pdf?job=build-docs "PDF: Community AAI Acceptable Use Policy (english)")
    [:fontawesome-solid-file-word:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05-VO_AUP_template.docx?job=build-docs "DOCX: Community AAI Acceptable Use Policy (english)")),
    (german: [:fontawesome-solid-file-pdf:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05-CAAI_AUP_DE.pdf?job=build-docs "PDF: Community AAI Acceptable Use Policy (german)")
    [:fontawesome-solid-file-word:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05-VO_AUP_template_DE.docx?job=build-docs "DOCX: Community AAI Acceptable Use Policy (german)"))
- VO Acceptable Use Policy (VO-AUP):
    english: ([:fontawesome-solid-file-pdf:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05-VO_AUP_template.pdf?job=build-docs "PDF: VO Acceptable Use Policy (english)")
    [:fontawesome-solid-file-word:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05-VO_AUP_template.docx?job=build-docs "DOCX: VO Acceptable Use Policy (english)")),
    (german: [:fontawesome-solid-file-pdf:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05-VO_AUP_template_DE.pdf?job=build-docs "PDF: VO Acceptable Use Policy (german)")
    [:fontawesome-solid-file-word:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05-VO_AUP_template_DE.docx?job=build-docs "DOCX: VO Acceptable Use Policy (german)"))
- Service Acceptable Use Policy (SAUP):
    english: ([:fontawesome-solid-file-pdf:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05a-Service_AUP_template.pdf?job=build-docs "PDF: Service Acceptable Use Policy (english)")
    [:fontawesome-solid-file-word:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05a-Service_AUP_template.docx?job=build-docs "DOCX: Service Acceptable Use Policy (english)")),
    (german: [:fontawesome-solid-file-pdf:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05a-Service_AUP_template_DE.pdf?job=build-docs "PDF: Service Acceptable Use Policy (german)")
    [:fontawesome-solid-file-word:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/05a-Service_AUP_template_DE.docx?job=build-docs "DOCX: Service Acceptable Use Policy (german)"))
- Service Access Policy (SAP):
    english: ([:fontawesome-solid-file-pdf:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/06_SAP_template.pdf?job=build-docs "PDF: Service Access Policy (english)")
    [:fontawesome-solid-file-word:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/06_SAP_template.docx?job=build-docs "DOCX: Service Access Policy (english)"))
- VO Lifecycle Policy:
    english: ([:fontawesome-solid-file-pdf:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/91_VO-lifecycle.pdf?job=build-docs "PDF: VO Lifecycle Policy (english)")
    [:fontawesome-solid-file-word:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/91_VO-lifecycle.docx?job=build-docs "DOCX: VO Lifecycle Policy (english)"))
- Privacy Statement Template:
    english: ([:fontawesome-solid-file-pdf:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/07_Privacy_Policy_Template.pdf?job=build-docs "PDF: Privacy Statement Template (english)")
    [:fontawesome-solid-file-word:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/07_Privacy_Policy_Template.docx?job=build-docs "DOCX: Privacy Statement Template (english)")),
    (german: [:fontawesome-solid-file-pdf:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/07_Privacy_Policy_Template_DE.pdf?job=build-docs "PDF: Privacy Statement Template (german)")
    [:fontawesome-solid-file-word:](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/07_Privacy_Policy_Template_DE.docx?job=build-docs "DOCX: Privacy Statement Template (german)"))
- Template Verfahrensverzeichnis:
    [odt](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.4/raw/Template_Verfahrensverzeichnis_AAI.odt?job=build-docs "ODT: Template Verfahrensverzeichnis")


## Underlying Policy Frameworks

The following Policy Frameworks / Recommendations are intended to be supported:

- [AARC Guidelines Documents Approved by AEGIS](https://wiki.geant.org/display/AARC/AARC+Documents+Approved+by+AEGIS)
- [REFEDS Assurance Framework](https://refeds.org/assurance)
- [REFEDS Data Protection Code of Conduct 2.0 (DPCoCo2)](https://refeds.org/category/code-of-conduct)
- [Security Incident Response Trust Framework for Federated Identity (Sirtfi)](https://refeds.org/sirtfi)

{% include 'gitinfo.md' %}
